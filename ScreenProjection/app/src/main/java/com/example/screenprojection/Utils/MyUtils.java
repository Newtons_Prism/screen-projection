package com.example.screenprojection.Utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MyUtils {
    public static final int  tcpSocketPort = 50000;
    public static final int  bitmapSocketPort = 50001;
    public static final int  touchSocketPort = 50002;
    public static final int  usbSocketPort = 50004;

    private static String mLocalIp;

    private static int mResultCode;
    private static Intent mResultData;
    private static Bitmap mBitmap;
    private static int mScreenWidth;
    private static int mScreenHeight;
    private static int mScreenDensity;
    private static int mBitmapWidth;
    private static int mBitmapHeight;

    private static int mStartX;
    private static int mStartY;
    private static int mEndX;
    private static int mEndY;

    private static int mKeyCode;


    public static String getLocalAddress() {
        try {
            for(Enumeration<NetworkInterface> networkInterfaceEnumeration =
                NetworkInterface.getNetworkInterfaces(); networkInterfaceEnumeration.hasMoreElements(); ) {
                NetworkInterface networkInterface = networkInterfaceEnumeration.nextElement();
                for(Enumeration<InetAddress> inetAddressEnumeration =
                    networkInterface.getInetAddresses(); inetAddressEnumeration.hasMoreElements(); ) {
                    InetAddress inetAddress = inetAddressEnumeration.nextElement();
                    if(inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "0.0.0.0";
    }

    public static String getIPv4Address(int ipAddress) {
        byte[] tempAddress = BigInteger.valueOf(ipAddress).toByteArray();
        int size = tempAddress.length;
        for(int i = 0; i < size/2; i++) {
            byte temp = tempAddress[size-1-i];
            tempAddress[size-1-i] = tempAddress[i];
            tempAddress[i] = temp;
        }
        try {
            InetAddress inetIP = InetAddress.getByAddress(tempAddress);
            return inetIP.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Bitmap BitmapSampleSizeCompress(final Bitmap rawBitmap, final int maxWidth, final int maxHeight) {
        if (rawBitmap == null || rawBitmap.getWidth() == 0 || rawBitmap.getHeight() == 0) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        rawBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        byte[] bytes = outputStream.toByteArray();
        BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        options.inSampleSize = CaculateinSampleSize(options, maxWidth, maxHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
    }

    private static int CaculateinSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int width = options.outWidth;
        final int height = options.outHeight;
        int inSampleSize = 1;
        if (width > reqWidth || height > reqHeight) {
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            inSampleSize = Math.min(heightRatio, widthRatio);
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public static Bitmap BitmapMatrixCompress(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setScale(0.5f, 0.5f);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static byte[] BitmaptoBytes(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();
    }

    public static Bitmap BytestoBitmap(byte[] b) {
        if(b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        } else {
            return null;
        }
    }

    public static String BytestoString(byte[] bytes) {
        String result = "";
        char tmp;
        int length = bytes.length;
        for (int i = 0; i < length; i++) {
            tmp = (char) bytes[i];
            result += tmp;
        }
        return result;
    }

    public static void ExecuteRunnable(Runnable runnable) {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        cachedThreadPool.execute(runnable);
    }

    public static void setResultCode(int resultCode) {
        mResultCode = resultCode;
    }

    public static int getResultCode() {
        return mResultCode;
    }

    public static void setResultData(Intent resultData) {
        mResultData = resultData;
    }

    public static Intent getResultData() {
        return mResultData;
    }

    public static void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public static Bitmap getBitmap() {
        return mBitmap;
    }

    public static void setScreenWidth(int width) {
        mScreenWidth = width;
    }

    public static void setScreenHeight(int height) {
        mScreenHeight = height;
    }

    public static void setScreenDensity(int density) {
        mScreenDensity = density;
    }

    public static int getScreenWidth() {
        return mScreenWidth;
    }

    public static int getScreenHeight() {
        return mScreenHeight;
    }

    public static int getScreenDensity() {
        return mScreenDensity;
    }

    public static void setBitmapWidth(int BitmapWidth) {
        mBitmapWidth = BitmapWidth;
    }

    public static int getBitmapWidth() {
        return mBitmapWidth;
    }

    public static void setBitmapHeight(int BitmapHeight) {
        mBitmapHeight = BitmapHeight;
    }

    public static int getBitmapHeight() {
        return mBitmapHeight;
    }

    public static void setLocalIp(String ip) {
        mLocalIp = ip;
    }

    public static String getLocalIp() {
        return mLocalIp;
    }

    public static void setStartX(int startX) {
        mStartX = startX;
    }

    public static int getStartX() {
        return mStartX;
    }

    public static void setStartY(int startY) {
        mStartY = startY;
    }

    public static int getStartY() {
        return mStartY;
    }

    public static void setEndX(int endX) {
        mEndX = endX;
    }

    public static int getEndX() {
        return mEndX;
    }

    public static void setEndY(int endY) {
        mEndY = endY;
    }

    public static int getEndY() {
        return mEndY;
    }

    public static void setKeyCode(int keyCode) {
        mKeyCode = keyCode;
    }

    public static int getKeyCode() {
        return mKeyCode;
    }
}
