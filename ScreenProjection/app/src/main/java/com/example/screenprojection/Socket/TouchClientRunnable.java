package com.example.screenprojection.Socket;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.screenprojection.Utils.MyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class TouchClientRunnable implements Runnable{

    private static final String TAG = "TouchClientRunnable";

    private Socket socket;
    private String ip;
    private int port;
    private boolean isTouchDown = false;
    private boolean isTouchUp = false;


    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setTouchDown(boolean isTouchDown) {
        this.isTouchDown = isTouchDown;
    }

    public void setTouchUp(boolean isTouchUp) {
        this.isTouchUp = isTouchUp;
    }

    @Override
    public void run() {
        ClientRun();
    }

    public void close() {
        ClientClose();
    }

    private boolean ClientConnect() {
        try {
            socket = new Socket(ip,port);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (listener != null) {
            listener.onClientConnect();
        }
        return true;
    }

    private boolean ClientIsConnect() {
        return socket != null && !socket.isClosed() && socket.isConnected();
    }

    private void ClientClose() {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listener != null) {
            listener.onClientClose();
        }
    }

    private void ClientSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private final static byte[] PACKAGE_HEAD = {(byte)0xFF, (byte)0xCF, (byte)0xFA, (byte)0xBF, (byte)0xF6, (byte)0xAF, (byte)0xFE, (byte)0xFF};

    private void ClientTransmitTouchPosition() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("staX", MyUtils.getStartX());
            jsonObject.put("staY", MyUtils.getStartY());
            jsonObject.put("endX", MyUtils.getEndX());
            jsonObject.put("endY", MyUtils.getEndY());
            jsonObject.put("keyC", MyUtils.getKeyCode());
            final String msg = jsonObject.toString();
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.write(PACKAGE_HEAD);
            dataOutputStream.writeInt(msg.getBytes(StandardCharsets.UTF_8).length);
            dataOutputStream.write(msg.getBytes(StandardCharsets.UTF_8));
            dataOutputStream.flush();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void ClientRun() {
        if (!ClientConnect()) {
            return;
        }
        while (true) {
            while (ClientIsConnect()) {
                if ((isTouchDown && isTouchUp) || MyUtils.getKeyCode() == 4) {
                    ClientTransmitTouchPosition();
                    isTouchDown = false;
                    isTouchUp = false;
                    MyUtils.setStartX(0);
                    MyUtils.setStartY(0);
                    MyUtils.setEndX(0);
                    MyUtils.setEndY(0);
                    MyUtils.setKeyCode(0);
                }
                ClientSleep(10);
            }
        }
    }

    private ClientListener listener;

    public interface ClientListener {
        void onClientConnect();
        void onClientClose();
    }

    public void setListener(ClientListener listener) {
        this.listener = listener;
    }
}
