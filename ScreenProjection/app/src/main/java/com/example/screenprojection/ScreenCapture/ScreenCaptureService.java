package com.example.screenprojection.ScreenCapture;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class ScreenCaptureService extends Service {

    private static final String TAG = "ScreenCaptureService";

    private ScreenCapture screenCapture;

    public ScreenCaptureService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            int resultCode = bundle.getInt("resultCode");
            Intent data = bundle.getParcelable("resultData");
            screenCapture = ScreenCapture.getInstance(this, resultCode, data);
        }
        screenCapture.startScreenCapture();
//        Toast.makeText(this,"开始捕获屏幕",Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        screenCapture.stopScreenCapture();
//        Toast.makeText(this,"停止捕获屏幕",Toast.LENGTH_SHORT).show();
    }
}