package com.example.screenprojection;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.DateKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.screenprojection.Display.DisplayActivity;
import com.example.screenprojection.Socket.BitmapServerRunnable;
import com.example.screenprojection.Socket.TcpClientRunnable;
import com.example.screenprojection.Utils.MyUtils;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class ClientFragment extends Fragment implements View.OnClickListener{

    private static final String TAG = "ClientFragment";

    private static boolean client_connect = false;

    private TextView client_edit;
    private Button connect_button;

    private TcpClientRunnable tcpClientRunnable;
    private BitmapServerRunnable bitmapServerRunnable;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_client, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        client_edit = (EditText) getActivity().findViewById(R.id.client_edit);
        client_edit.setText(MyUtils.getLocalIp());

        connect_button = (Button) getActivity().findViewById(R.id.connect_button);
        connect_button.setOnClickListener(this);

        tcpClientRunnable = new TcpClientRunnable();
        tcpClientRunnable.setPort(MyUtils.tcpSocketPort);
        tcpClientRunnable.setListener(tcpClientListener);

        bitmapServerRunnable = new BitmapServerRunnable();
        bitmapServerRunnable.setPort(MyUtils.bitmapSocketPort);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.connect_button) {
            if (client_edit.getText().toString().trim().length() == 0) {
                Toast.makeText(getActivity(), "请输入服务端IP", Toast.LENGTH_SHORT).show();
            } else {
                if (!client_connect) {
                    client_connect = true;
                    tcpClientRunnable.setIp(client_edit.getText().toString().trim());
                    MyUtils.ExecuteRunnable(tcpClientRunnable);
                    MyUtils.ExecuteRunnable(bitmapServerRunnable);
                    client_edit.setText(MyUtils.getLocalIp());
                    client_edit.setEnabled(false);
                    connect_button.setText("断开");
                } else {
                    client_connect = false;
                    tcpClientRunnable.close();
                    bitmapServerRunnable.close();
                    client_edit.setText(client_edit.getText().toString().trim());
                    client_edit.setEnabled(true);
                    connect_button.setText("连接");
                }
            }
        }
    }

    TcpClientRunnable.ClientListener tcpClientListener = new TcpClientRunnable.ClientListener() {
        @Override
        public void onClientConnect() {
            Intent intent = new Intent(getActivity(), DisplayActivity.class);
            startActivity(intent);
        }

        @Override
        public void onClientClose() {

        }

        @Override
        public void onClientReceiveBitmap(Bitmap bitmap, int width, int height) {
            bitmapServerRunnable.setBitmap(bitmap, width, height);
        }
    };
}
