package com.example.screenprojection.ScreenCapture;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.screenprojection.Utils.MyUtils;

public class BitmapProcessRunnable implements Runnable{

    private static final String TAG = "BitmapProcessRunnable";

    private boolean isRun = false;

    public void setRun(boolean isRun) {
        this.isRun = isRun;
    }

    @Override
    public void run() {
        while (isRun) {
            try {
                Bitmap bitmap = MyUtils.getBitmap();
                if (bitmap != null) {
                    bitmap = MyUtils.BitmapMatrixCompress(bitmap);
                    if (listener != null) {
                        listener.onProcessBitmap(bitmap);
                    }
                }
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private ProcessListener listener;

    public interface ProcessListener {
        void onProcessBitmap(Bitmap bitmap);
    }

    public void setListener(ProcessListener listener) {
        this.listener = listener;
    }
}
