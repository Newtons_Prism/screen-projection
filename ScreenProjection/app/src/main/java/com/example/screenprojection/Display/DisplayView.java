package com.example.screenprojection.Display;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.example.screenprojection.Socket.TouchClientRunnable;
import com.example.screenprojection.Utils.MyUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DisplayView extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, Runnable{

    private static final String TAG = "DisplayView";

    private int viewWidth;
    private int viewHeight;
    private Bitmap bitmap;
    private int width;
    private int height;
    private SurfaceHolder surfaceHolder;
    private boolean isDraw = false;
    private TouchClientRunnable touchClientRunnable;


    public DisplayView(Context context) {
        super(context);
        initView();
    }

    public DisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public DisplayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setFormat(PixelFormat.TRANSLUCENT);
        setZOrderMediaOverlay(true);
        touchClientRunnable = new TouchClientRunnable();
        touchClientRunnable.setIp(MyUtils.getLocalIp());
        touchClientRunnable.setPort(MyUtils.touchSocketPort);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        isDraw = true;
        MyUtils.ExecuteRunnable(this);
        MyUtils.ExecuteRunnable(touchClientRunnable);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        isDraw = false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
//                Log.d(TAG, "start："+motionEvent.getX()+","+motionEvent.getY());
                int staX = (int) (motionEvent.getX() * getWidthConvert());
                int staY = (int) (motionEvent.getY() * getHeightConvert());
                MyUtils.setStartX(staX);
                MyUtils.setStartY(staY);
//                Log.d(TAG, "start："+staX+","+staY);
                touchClientRunnable.setTouchDown(true);
                break;
            case MotionEvent.ACTION_UP:
//                Log.d(TAG, "end："+motionEvent.getX()+","+motionEvent.getY());
                int endX = (int) (motionEvent.getX() * getWidthConvert());
                int endY = (int) (motionEvent.getY() * getHeightConvert());
                MyUtils.setEndX(endX);
                MyUtils.setEndY(endY);
//                Log.d(TAG, "end："+endX+","+endY);
                touchClientRunnable.setTouchUp(true);
                break;
        }
        return true;
    }

    @Override
    public void run() {
        while (isDraw) {
            try {
                drawBitmap();
                setOnTouchListener(this);
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void drawBitmap() {
        Canvas canvas = surfaceHolder.lockCanvas();
        if (canvas != null) {
            bitmap = getBitmap();
            if (bitmap != null) {
                canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                Rect rect = new Rect(0, 0, viewWidth, viewHeight);
                canvas.drawBitmap(bitmap, null, rect, null);
            }
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void setBitmap(Bitmap bitmap, int width, int height) {
        this.bitmap = bitmap;
        this.width = width;
        this.height = height;
    }

    public float getWidthConvert() {
        float result;
        if (MyUtils.getScreenWidth() < width) {
            result = (float) width / (float) MyUtils.getScreenWidth();
        } else if (MyUtils.getScreenWidth() > width) {
            result = (float) width / (float) MyUtils.getScreenWidth();
        } else {
            result = 1;
        }
//        Log.d(TAG, "result1:"+result);
        return result;
    }

    public float getHeightConvert() {
        float result;
        if (MyUtils.getScreenHeight() < height) {
            result = (float) height / (float) MyUtils.getScreenHeight();
        } else if (MyUtils.getScreenHeight() > height) {
            result = (float) height / (float) MyUtils.getScreenHeight() ;
        } else {
            result = 1;
        }
//        Log.d(TAG, "result2:"+result);
        return result;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setViewWidth(int width) {
        this.viewWidth = width;
    }

    public void setViewHeight(int height) {
        this.viewHeight = height;
    }
}
