﻿#include "libusb_adb.h"


int main(void)
{
	int r;
	adb_handle* handle = (adb_handle*)malloc(sizeof(adb_handle));
	r = libusb_init(NULL);
	if (r < 0) {
		printf("libusb_init faild\r\n");
		return -1;
	}
	match_with_interface(handle);
	if (handle->state != INIT_DONE) {
		printf("match_with_interface faild\r\n");
		return -1;
	}
	while (1) {
		switch (handle->state) {

		case INIT_DONE:
			handle->state = SEND_AUTH;
			break;

		case SEND_AUTH: //发送AUTH KEY 申请USB调试权限
			r = send_auth_remote(handle, AUTH_KEY);
			if (r < 0) {
				printf("*** send_auth_remote failed! \n");
				break;
			}
			printf("SEND_AUTH SUCCESS\n");
			handle->state = SEND_CMD;
			break;

		case SEND_CMD:
			r = send_cmd_remote(handle, TOUCH_CMD); //反向控制脚本
			if (r < 0) {
				printf("get_logcat_remote faild\r\n");
				break;
			}
			printf("SEND_CMD SUCCESS\n");
			handle->state = IDLE;
			break;

		default:
			break;

		}
	}
	libusb_exit(NULL);
	free(handle);
	return 0;
}

//int main(void)
//{
//	int r;
//	char ch;
//	adb_handle* handle = (adb_handle*)malloc(sizeof(adb_handle));
//	r = libusb_init(NULL);
//	if (r < 0) {
//		printf("libusb_init faild\r\n");
//		return -1;
//	}
//	match_with_interface(handle);
//	if (handle->state != INIT_DONE) {
//		printf("match_with_interface faild\r\n");
//		return -1;
//	}
//	while (1) {
//		switch (handle->state) {
//			case INIT_DONE:
//				printf("start get cmd\n");
//				handle->state = SEND_AUTH;
//				break;
//			case SEND_AUTH: //发送AUTH KEY 申请USB调试权限
//				r = send_auth_remote(handle, AUTH_KEY);
//				if (r < 0) {
//					printf("*** send_auth_remote failed! \n");
//					break;
//				}
//				handle->state = SEND_CMD;
//				break;
//			case SEND_CMD:
//				printf("请输入字符'a'测试：");
//				scanf("%c", &ch);
//				getchar();
//				switch (ch)
//				{
//					case 'a':
//						r = send_cmd_remote(handle, "shell:input keyevent 26"); //熄屏命令
//						if (r < 0) {
//							printf("get_logcat_remote faild\r\n");
//						}
//						break;
//					default:
//						break;
//				}
//			default:
//				break;
//		}
//	}
//	libusb_exit(NULL);
//	free(handle);
//	return 0;
//}


