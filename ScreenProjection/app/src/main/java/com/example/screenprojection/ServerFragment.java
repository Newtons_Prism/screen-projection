package com.example.screenprojection;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.screenprojection.Socket.TouchServerRunnable;
import com.example.screenprojection.Socket.TcpServerRunnable;
import com.example.screenprojection.ScreenCapture.BitmapProcessRunnable;
import com.example.screenprojection.ScreenCapture.ScreenCaptureService;
import com.example.screenprojection.Utils.MyUtils;

import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class ServerFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ServerFragment";

    private static boolean server_create = false;

    private TextView server_text;
    private Button create_button;

    private TcpServerRunnable tcpServerRunnable;
    private BitmapProcessRunnable bitmapProcessRunnable;
    private TouchServerRunnable touchServerRunnable;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_server, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        server_text = (TextView) getActivity().findViewById(R.id.server_text);
        server_text.setText(MyUtils.getLocalAddress());

        create_button = (Button) getActivity().findViewById(R.id.create_button);
        create_button.setOnClickListener(this);

        bitmapProcessRunnable = new BitmapProcessRunnable();
        bitmapProcessRunnable.setListener(bitmapProcessListener);

        tcpServerRunnable = new TcpServerRunnable();
        tcpServerRunnable.setPort(MyUtils.tcpSocketPort);
        tcpServerRunnable.setListener(tcpServerListener);

        touchServerRunnable = new TouchServerRunnable();
        touchServerRunnable.setPort(MyUtils.touchSocketPort);
        touchServerRunnable.setListener(touchServerListener);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.create_button) {
            if (!server_create) {
                server_create = true;
                MyUtils.ExecuteRunnable(tcpServerRunnable);
                MyUtils.ExecuteRunnable(touchServerRunnable);
                create_button.setText("关闭");
            } else {
                server_create = false;
                tcpServerRunnable.close();
                touchServerRunnable.close();
                create_button.setText("创建");
            }
        }
    }

    BitmapProcessRunnable.ProcessListener bitmapProcessListener = new BitmapProcessRunnable.ProcessListener() {
        @Override
        public void onProcessBitmap(Bitmap bitmap) {
            tcpServerRunnable.setBitmap(bitmap);
        }
    };

    TcpServerRunnable.ServerListener tcpServerListener = new TcpServerRunnable.ServerListener() {
        @Override
        public void onServerConnect() {
            bitmapProcessRunnable.setRun(true);
            MyUtils.ExecuteRunnable(bitmapProcessRunnable);
            Bundle bundle = new Bundle();
            Intent start = new Intent(getActivity(), ScreenCaptureService.class);
            bundle.putInt("resultCode", MyUtils.getResultCode());
            bundle.putParcelable("resultData", MyUtils.getResultData());
            start.putExtras(bundle);
            Objects.requireNonNull(getActivity()).startService(start);
        }

        @Override
        public void onServerClose() {
            bitmapProcessRunnable.setRun(false);
            Intent stop = new Intent(getActivity(), ScreenCaptureService.class);
            Objects.requireNonNull(getActivity()).stopService(stop);
        }
    };

    TouchServerRunnable.ServerListener touchServerListener = new TouchServerRunnable.ServerListener() {
        @Override
        public void onServerConnect() {

        }

        @Override
        public void onServerClose() {

        }

        @Override
        public void onServerReceiveTouchPosition(int staX, int staY, int endX, int endY, int keyC) {
            if (keyC == KeyEvent.KEYCODE_BACK) {
                Log.i(TAG, "@" + "input keyevent 4");
            } else if ((staX - endX) == 0 && (staY - endY) == 0) {
                Log.i(TAG, "@" + "input tap" + " " + staX + " " + staY);
            } else if (Math.abs(staX - endX) > 100 || Math.abs(staY - endY) > 100) {
                Log.i(TAG, "@" + "input swipe" + " " + staX + " " + staY + " " + endX + " " + endY);
            }
        }
    };
}
